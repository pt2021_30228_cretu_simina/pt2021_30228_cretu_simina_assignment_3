package ro.tuc.tp.bll;

import ro.tuc.tp.dao.ClientDAO;
import ro.tuc.tp.model.Client;

/**
 * In this class are made the calls for the query methods for the Client table.
 */
public class ClientBLL {
    /**
     * This method calls the insert method in ClientDAO for inserting an client into the database
     */
    public void insert(Client client) {
        ClientDAO clientDAO = new ClientDAO();
        clientDAO.insert(client);
    }
    public void delete(Client client) {
        ClientDAO clientDAO = new ClientDAO();
        clientDAO.delete(client);
    }
    public void update(Client client) {
        ClientDAO clientDAO = new ClientDAO();
        clientDAO.update(client);
    }
}
