package ro.tuc.tp.bll;

import ro.tuc.tp.dao.ComandaDAO;
import ro.tuc.tp.model.Comanda;

/**
 * In this class are made the calls for the query methods for the Comanda table.
 */
public class ComandaBLL {
    /**
     * This method calls the insert method in ComandaDAO for inserting an order into the database
     */
    public void insert(Comanda comanda) {
        ComandaDAO comandaDAO = new ComandaDAO();
        comandaDAO.insert(comanda);
    }
    public void delete(Comanda comanda) {
        ComandaDAO comandaDAO = new ComandaDAO();
        comandaDAO.delete(comanda);
    }
    public void update(Comanda comanda) {
        ComandaDAO comandaDAO = new ComandaDAO();
        comandaDAO.update(comanda);
    }
}
