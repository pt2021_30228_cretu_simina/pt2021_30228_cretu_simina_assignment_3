package ro.tuc.tp.bll;

import ro.tuc.tp.dao.ProdusDAO;
import ro.tuc.tp.model.Produs;


/**
 * In this class are made the calls for the query methods for the Products table.
 */
public class ProdusBLL{
    /**
     * This method calls the insert method in ProdusDAO for inserting an product into the database
     */
    private ProdusDAO produsDAO = new ProdusDAO();
    public void insert(Produs produs) {
        produsDAO.insert(produs);
    }
    public void delete(Produs produs) {
        produsDAO.delete(produs);
    }
    public void update(Produs produs) {
        produsDAO.update(produs);
    }
}
