package ro.tuc.tp.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class makes the connection to the database
 */
public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL= "jdbc:mysql://localhost:3306/ordermanagement?autoReconnect=true&useSSL=false";
    private static final String USER = "root";
    private static final String PASS = "root";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private ConnectionFactory() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection createConnection(){
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ordermanagement", "root", "Root*1Root");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "An error occurred while trying to connect to the database");
            e.printStackTrace();
        }
        return connection;
    }

    public static Connection getConnection(){
        return singleInstance.createConnection();
    }


    public static void close(Connection connection){
        try{
            if(connection!=null)
                connection.close();
        }catch (SQLException se){
            LOGGER.log(Level.WARNING, "An error occurred while trying to close the connection");
        }

    }
    public static void close(Statement statement){
        try{
            if(statement!=null)
                statement.close();
        }catch (SQLException se){
            LOGGER.log(Level.WARNING, "An error occurred while trying to close the statement");
        }
    }
    public static void close(ResultSet resultSet){
        try{
            if(resultSet!=null)
                resultSet.close();
        }catch (SQLException se){
            LOGGER.log(Level.WARNING, "An error occurred while trying to close the ResultSet");
        }
    }
}