package ro.tuc.tp.dao;

import ro.tuc.tp.connection.ConnectionFactory;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The class that accesses the database
 */
public class AbstractDAO<T> {

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private String viewAllQuery(String field) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT ");
        stringBuilder.append(" * ");
        stringBuilder.append(" FROM ");
        stringBuilder.append("ordermanagement."+ type.getSimpleName());
        stringBuilder.append(" WHERE " + field + " = ? ");

        return stringBuilder.toString();
    }

    public String insertQuery(String[] fields) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" INSERT INTO ");
        stringBuilder.append("ordermanagement." + type.getSimpleName());
        stringBuilder.append(" (");
        for (int i = 0, fieldsLength = fields.length; i < fieldsLength; i++) {
            String field = fields[i];
            stringBuilder.append(field + ",");
        }
        stringBuilder.append(") VALUES (");
        for (int i = 0, fieldsLength = fields.length; i < fieldsLength; i++) {
            String field = fields[i];
            stringBuilder.append("?,");
        }
        stringBuilder.append(")");

        return stringBuilder.toString().replace(",)", ")");
    }

    public String deleteQuery(String field) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DELETE FROM ");
        stringBuilder.append("ordermanagement." + type.getSimpleName());
        stringBuilder.append(" WHERE " + field + "=?");

        return stringBuilder.toString();
    }

    public String updateQuery(String[] fields) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE ");
        stringBuilder.append("ordermanagement." + type.getSimpleName());
        stringBuilder.append(" SET ");
        for (String field : fields) {
            stringBuilder.append(field + "=?,");
        }
        stringBuilder.append(") WHERE " + fields[0] + "=?");

        return stringBuilder.toString().replace(",)", "");
    }


    /**
     * Create a list of objects after the result of a query.
     */
    private List<T> createObjects(ResultSet rs) {
        Constructor constructor = null;
        Constructor[] constructors = type.getDeclaredConstructors();
        List<T> list = new ArrayList<T>();

        int i = 0;
        while (i < constructors.length) {
            constructor = constructors[i];
            if (constructor.getGenericParameterTypes().length == 0)
                break;
            i++;
        }
        try {
            while ( rs.next()){
                constructor.setAccessible(true);
                T instance = (T) constructor.newInstance();
                for ( Field field : type.getDeclaredFields()){
                    String string = field.getName();
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(string, type);
                    Object object = rs.getObject(string);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, object);
                }
                list.add(instance);
            }
        }catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException | SecurityException | IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void insert(T t) {
        Connection con = null;
        PreparedStatement sts = null;
        Field[] fieldInsert = type.getDeclaredFields();
        String[] strings = new String[fieldInsert.length];;

        try{
            int i;
            i=0;
            while (i < type.getDeclaredFields().length) {
                strings[i] = fieldInsert[i].getName();
                fieldInsert[i].setAccessible(true);
                i++;
            }
            con = ConnectionFactory.getConnection();
            String string = insertQuery(strings);
            sts = con.prepareStatement(string);

            i=0;
            while (i < type.getDeclaredFields().length) {
                Object object = fieldInsert[i].get(t);
                sts.setString(i + 1, object.toString());
                i++;
            }

            sts.executeUpdate();
        }catch(SQLException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void delete(T t) {
        Connection con = null;
        PreparedStatement sts = null;
        Field[] deleteField = type.getDeclaredFields();
        String[] strings = new String[deleteField.length];

        try{
            int i;
            i=0;
            while (i < type.getDeclaredFields().length) {
                strings[i] = deleteField[i].getName();
                i++;
            }
            deleteField[0].setAccessible(true);

            con = ConnectionFactory.getConnection();
            String string = deleteQuery(strings[0]);
            sts = con.prepareStatement(string);

            Object object = deleteField[0].get(t);
            sts.setString(1, object.toString());
            sts.executeUpdate();
        }catch (SQLException | IllegalAccessException e){
            e.printStackTrace();
        }
    }

    public void update(T t) {
        Connection con = null;
        PreparedStatement sts = null;
        Field[] updateField = type.getDeclaredFields();
        String[] strings = new String[updateField.length];

        try{
            int i;
            i=0;
            while (i < type.getDeclaredFields().length) {
                strings[i] = updateField[i].getName();
                updateField[i % updateField.length].setAccessible(true);
                i++;
            }
            con = ConnectionFactory.getConnection();
            String string = updateQuery(strings);
            sts = con.prepareStatement(string);

            i=0;
            while (i <= type.getDeclaredFields().length) {
                Object object = updateField[i % updateField.length].get(t);
                sts.setString(i + 1, object.toString());
                i++;
            }
            sts.executeUpdate();
        }catch ( SQLException | IllegalAccessException e){
            e.printStackTrace();
        }
    }

    /**
     * Search all objects in the database.
     */

    public List<T> ViewAll() {
        Connection con = null;
        PreparedStatement sts = null;
        ResultSet rs = null;
        String query = viewAllQuery("1");

        try{
            con = ConnectionFactory.getConnection();
            sts = con.prepareStatement(query);
            sts.setInt(1, 1);
            rs = sts.executeQuery();
            return createObjects(rs);

        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(sts);
            ConnectionFactory.close(con);
        }
        return null;
    }

    /**
     * Creates a table with the values in the database
     */
    public JTable createTable(List<T> objects) throws IllegalArgumentException, IllegalAccessException {
        ArrayList<String> columnNames = new ArrayList<String>();

        for(Field field : objects.get(0).getClass().getDeclaredFields()) {
            field.setAccessible(true);
            columnNames.add(field.getName());
        }
        String[] column = new String[columnNames.size()];
        column = columnNames.toArray(column);
        DefaultTableModel tableModel = new DefaultTableModel(column, 0);
        Iterator<T> iterator = objects.iterator();

        while(iterator.hasNext()) {
            T object = iterator.next();
            ArrayList<Object> columnData = new ArrayList<Object>();

            for(Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                columnData.add(field.get(object));
            }
            Object[] columnDataAsArray = new Object[columnData.size()];
            columnDataAsArray = columnData.toArray(columnDataAsArray);
            tableModel.addRow(columnDataAsArray);
        }
        JTable jTable = new JTable(tableModel);
        return jTable;
    }
}
