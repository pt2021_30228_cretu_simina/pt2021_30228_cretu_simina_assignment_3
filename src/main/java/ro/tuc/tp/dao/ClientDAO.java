package ro.tuc.tp.dao;

import ro.tuc.tp.model.Client;

import javax.swing.*;
import java.util.List;

/**
 * This class is responsible for the queries that interrogate the table Client in the database.
 */

public class ClientDAO extends AbstractDAO<Client>{
    @Override
    public void insert(Client client) {
        super.insert(client);
    }

    @Override
    public void update(Client client) {
        super.update(client);
    }

    @Override
    public void delete(Client client) {
        super.delete(client);
    }

    @Override
    public JTable createTable(List<Client> objects) throws IllegalArgumentException, IllegalAccessException {
        return super.createTable(objects);
    }

    @Override
    public List<Client> ViewAll() {
        return super.ViewAll();
    }
}
