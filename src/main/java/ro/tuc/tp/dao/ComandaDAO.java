package ro.tuc.tp.dao;

import ro.tuc.tp.model.Comanda;

import javax.swing.*;
import java.util.List;

/**
 * This class is responsible for the queries that interrogate the table Order in the database.
 */

public class ComandaDAO extends AbstractDAO<Comanda>{
    @Override
    public void insert(Comanda comanda) {
        super.insert(comanda);
    }

    @Override
    public void update(Comanda comanda) {
        super.update(comanda);
    }

    @Override
    public void delete(Comanda comanda) {
        super.delete(comanda);
    }

    @Override
    public JTable createTable(List<Comanda> objects) throws IllegalArgumentException, IllegalAccessException {
        return super.createTable(objects);
    }

    @Override
    public List<Comanda> ViewAll() {
        return super.ViewAll();
    }
}
