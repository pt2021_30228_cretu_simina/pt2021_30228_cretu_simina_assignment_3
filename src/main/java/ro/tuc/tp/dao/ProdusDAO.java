package ro.tuc.tp.dao;

import ro.tuc.tp.connection.ConnectionFactory;
import ro.tuc.tp.model.Produs;

import javax.swing.*;
import java.sql.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * This class is responsible for the queries that interrogate the table Produs in the database.
 */

public class ProdusDAO extends AbstractDAO<Produs> {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());

    @Override
    public void insert(Produs produs) {
        super.insert(produs);
    }

    @Override
    public void update(Produs produs) {
        super.update(produs);
    }

    @Override
    public void delete(Produs produs) {
        super.delete(produs);
    }

    @Override
    public JTable createTable(List<Produs> objects) throws IllegalArgumentException, IllegalAccessException {
        return super.createTable(objects);
    }

    @Override
    public List<Produs> ViewAll() {
        return super.ViewAll();
    }

    /**
     * This method finds a product by name.
     */
    public static Produs findbyname(String nume){
        Produs toReturn = null;
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM produs WHERE nume = ?";
        try{
            pst = (PreparedStatement) con.prepareStatement(sql);
            pst.setString(1, nume);
            rs = pst.executeQuery();
            rs.next();
            int idProdus = rs.getInt("idProdus");
            int stoc = rs.getInt("stoc");
            int pret = rs.getInt("pret");
            toReturn = new Produs(idProdus,nume,pret,stoc);
        }catch(SQLException e){
            LOGGER.log(Level.WARNING,"Produs:findbyname" + e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(pst);
            ConnectionFactory.close(con);
        }
        return toReturn;
    }

    /**
     * This method edits the stock of a product.
     */
    public static void editareStoc(int stoc, String numeProdus){
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement pst = null;
        String sql = "UPDATE produs SET stoc = ? WHERE nume = ?";
        try{
            pst=(PreparedStatement) con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setInt(1,stoc);
            pst.setString(2,numeProdus);
            pst.executeUpdate();
        }catch(SQLException e){
            LOGGER.log(Level.WARNING, "Produs" + e.getMessage());
        }finally {
            ConnectionFactory.close(pst);
            ConnectionFactory.close(con);
        }
    }
}
