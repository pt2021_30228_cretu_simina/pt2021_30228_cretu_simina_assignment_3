package ro.tuc.tp.model;

/**
 * This class simulates the clients, and has associated with it a sql table with the data
 */
public class Client {
    private int idClient;
    private String nume;
    private String adresa;

    /**
     *
     * @param nume Your full name which including the first name and last name.
     * @param adresa Your home address.
     */
    public Client(int idClient, String nume, String adresa){
        this.idClient = idClient;
        this.nume = nume;
        this.adresa = adresa;
    }

    public Client() {

    }

    public int getIdClient(){
        return idClient;
    }

    public void setIdClient(int idClient){
        this.idClient = idClient;
    }

    public String getNume(){
        return nume;
    }

    public void setNume(String nume){
        this.nume = nume;
    }

    public String getAdresa(){
        return adresa;
    }

    public void setAdresa(String adresa){
        this.adresa = adresa;
    }

    public String toString(){
        return "Client: id =" + idClient + ", nume = " + nume + ", adresa = " + adresa ;
    }
}
