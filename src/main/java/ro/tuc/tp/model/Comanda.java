package ro.tuc.tp.model;

/**
 * This class simulates the orders, and has associated with it a sql table with the data
 */
public class Comanda {
    private int idComanda;
    private String numeClient;
    private String numeProdus;
    private int cantitate;

    /**
     * @param numeClient The name of the customer placing the order.
     * @param numeProdus The name of the desired product.
     * @param cantitate Number of products.
     */
    public Comanda (int idComanda, String numeClient, String numeProdus, int cantitate){
        this.idComanda = idComanda;
        this.numeClient = numeClient;
        this.numeProdus = numeProdus;
        this.cantitate = cantitate;
    }

    public Comanda() {

    }

    public int getIdComanda(){
        return idComanda;
    }

    public void setIdComanda(int idComanda){
        this.idComanda = idComanda;
    }

    public String getNumeClient(){
        return numeClient;
    }

    public void setNumeClient(String numeClient){
        this.numeClient = numeClient;
    }

    public String getNumeProdus(){
        return numeProdus;
    }

    public void setNumeProdus(String numeProdus){
        this.numeProdus = numeProdus;
    }

    public int getCantitate(){
        return cantitate;
    }

    public void setCantitate(int cantitate){
        this.cantitate = cantitate;
    }

    public String toString(){
        return "Comanda: id =" + idComanda + ", nume client = " + numeClient + ", nume produs = " + numeProdus + ", cantitate = " + cantitate;
    }
}
