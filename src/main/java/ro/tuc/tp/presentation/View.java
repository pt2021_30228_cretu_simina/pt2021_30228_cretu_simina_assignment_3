package ro.tuc.tp.presentation;

import ro.tuc.tp.bll.ClientBLL;
import ro.tuc.tp.bll.ComandaBLL;
import ro.tuc.tp.bll.ProdusBLL;
import ro.tuc.tp.connection.ConnectionFactory;
import ro.tuc.tp.dao.ClientDAO;
import ro.tuc.tp.dao.ComandaDAO;
import ro.tuc.tp.dao.ProdusDAO;
import ro.tuc.tp.model.Client;
import ro.tuc.tp.model.Comanda;
import ro.tuc.tp.model.Produs;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * This class implements the graphical user interface
 */
public class View extends javax.swing.JFrame{

    public View() {
        initComponents();
    }

    private void retrieveProdus() throws IllegalAccessException {
        ProdusDAO produsDAO = new ProdusDAO();
        List<Produs> produsList = produsDAO.ViewAll();
        produsjTable1 = produsDAO.createTable(produsList);
    }

    private void retrieveClient() throws IllegalAccessException {
        ClientDAO clientDAO = new ClientDAO();
        List<Client> clientList = clientDAO.ViewAll();
        clientjTable1 = clientDAO.createTable(clientList);
    }

    private void retrieveComanda() throws IllegalAccessException {
        ComandaDAO comandaDAO = new ComandaDAO();
        List<Comanda> comandaList = comandaDAO.ViewAll();
        comandajTable1 = comandaDAO.createTable(comandaList);
    }


    @SuppressWarnings("unchecked")
    private void initComponents() {

        jframe = new javax.swing.JFrame();
        produsBtn = new javax.swing.JButton();
        produsJFrame = new javax.swing.JFrame();
        produsjScrollPane1 = new javax.swing.JScrollPane(produsjTable1);
        produsjTable1 = new javax.swing.JTable();
        idProdus = new javax.swing.JTextField();
        produsjLabel1 = new javax.swing.JLabel();
        produsjLabel2 = new javax.swing.JLabel();
        produsjLabel3 = new javax.swing.JLabel();
        produsRetrieve = new javax.swing.JButton();
        produsAdd = new javax.swing.JButton();
        numeProdus = new javax.swing.JTextField();
        stocProdus = new javax.swing.JTextField();
        pretProdus = new javax.swing.JTextField();
        produsUpdate = new javax.swing.JButton();
        produsDelete = new javax.swing.JButton();
        produsClear = new javax.swing.JButton();
        produsjLabel4 = new javax.swing.JLabel();

        clientBtn = new javax.swing.JButton();
        clientJFrame = new javax.swing.JFrame();
        clientjScrollPane1 = new javax.swing.JScrollPane(clientjTable1);
        clientjTable1 = new javax.swing.JTable();
        idClient = new javax.swing.JTextField();
        clientjLabel1 = new javax.swing.JLabel();
        clientjLabel2 = new javax.swing.JLabel();
        clientjLabel3 = new javax.swing.JLabel();
        clientRetrieve = new javax.swing.JButton();
        clientAdd = new javax.swing.JButton();
        numeClient = new javax.swing.JTextField();
        adresaClient = new javax.swing.JTextField();
        clientUpdate = new javax.swing.JButton();
        clientDelete = new javax.swing.JButton();
        clientClear = new javax.swing.JButton();
        clientjLabel4 = new javax.swing.JLabel();

        comandaBtn = new javax.swing.JButton();
        comandaJFrame = new javax.swing.JFrame();
        comandajScrollPane1 = new javax.swing.JScrollPane(comandajTable1);
        comandajTable1 = new javax.swing.JTable();
        idComanda = new javax.swing.JTextField();
        comandajLabel1 = new javax.swing.JLabel();
        comandajLabel2 = new javax.swing.JLabel();
        comandajLabel3 = new javax.swing.JLabel();
        comandaRetrieve = new javax.swing.JButton();
        comandaAdd = new javax.swing.JButton();
        numeClientComada = new javax.swing.JTextField();
        numeProdusComanda = new javax.swing.JTextField();
        comandaUpdate = new javax.swing.JButton();
        comandaDelete = new javax.swing.JButton();
        comandaClear = new javax.swing.JButton();
        comandajLabel4 = new javax.swing.JLabel();
        cantitateComanda = new javax.swing.JTextField();
        comandajComboBox = new javax.swing.JComboBox<>();
        comandaClientCombo = new javax.swing.JComboBox<>();

        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.setSize(700, 500);
        jframe.setLayout(null);
        jframe.setVisible(true);
        jframe.setTitle("Order Management");
        jframe.add(produsBtn);
        jframe.add(clientBtn);
        jframe.add(comandaBtn);
        produsBtn.setText("Produs");
        produsBtn.setBounds(100, 100, 400, 40);
        produsBtn.setFocusable(false);
        produsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                produsBtnActionPerformed(evt);
            }
        });
        clientBtn.setText("Client");
        clientBtn.setBounds(100, 160, 400, 40);
        clientBtn.setFocusable(false);
        clientBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientBtnActionPerformed(evt);
            }
        });
        comandaBtn.setText("Comanda");
        comandaBtn.setBounds(100,220,400,40);
        comandaBtn.setFocusable(false);
        comandaBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comandaBtnActionPerformed(evt);
            }
        });
    }

    public void client() {
        clientJFrame.add(clientjLabel1);
        clientJFrame.add(clientjLabel2);
        clientJFrame.add(clientjLabel3);
        clientJFrame.add(clientjLabel4);
        clientJFrame.add(idClient);
        clientJFrame.add(numeClient);
        clientJFrame.add(adresaClient);
        clientJFrame.add(clientAdd);
        clientJFrame.add(clientUpdate);
        clientJFrame.add(clientRetrieve);
        clientJFrame.add(clientClear);
        clientJFrame.add(clientDelete);
        clientJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        clientJFrame.setSize(700, 500);
        clientJFrame.setLayout(null);
        clientJFrame.setVisible(true);
        clientJFrame.setTitle("Client");

        clientjTable1.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                },
                new String [] {
                }
        ));
        clientjTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                clientjTable1MouseClicked(evt);
            }
        });

        clientjScrollPane1.setViewportView(clientjTable1);
        clientjLabel1.setText("idClient");
        clientjLabel1.setBounds(450,40,100,50);
        clientjLabel2.setText("Nume");
        clientjLabel2.setBounds(450,80,100,50);
        clientjLabel3.setText("adresa");
        clientjLabel3.setBounds(450,120,100,50);
        clientjLabel4.setText("Pret");

        idClient.setBounds(510,55,150,20);
        numeClient.setBounds(510,95,150,20);
        adresaClient.setBounds(510,135,150,20);

        clientAdd.setBounds(510,215,150,35);
        clientUpdate.setBounds(510,255,150,35);
        clientDelete.setBounds(510,295,150,35);
        clientRetrieve.setBounds(510,335,150,35);
        clientClear.setBounds(510,375,150,35);

        clientjTable1.setBounds(10,10,400,400);
        clientjScrollPane1.setBounds(10,10,400,400);

        clientRetrieve.setText("Retrieve");
        clientRetrieve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) clientjTable1.getModel();
                model.setRowCount(0);
                try {
                    retrieveClient();
                    clientjScrollPane1.setViewportView(clientjTable1);

                    clientJFrame.add(clientjScrollPane1);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        clientAdd.setText("Add");
        clientAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) clientjTable1.getModel();
                model.setRowCount(0);
                try {
                    clientjScrollPane1.setViewportView(clientjTable1);

                    clientJFrame.add(clientjScrollPane1);
                    clientAddBtnActionPerformed(evt);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        clientUpdate.setText("Update");
        clientUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) clientjTable1.getModel();
                model.setRowCount(0);
                try {
                    clientjScrollPane1.setViewportView(clientjTable1);

                    clientJFrame.add(clientjScrollPane1);
                    clientUpdateBtnActionPerformed(evt);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        });

        clientDelete.setText("Delete");
        clientDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) clientjTable1.getModel();
                model.setRowCount(0);
                try {
                    clientjScrollPane1.setViewportView(clientjTable1);

                    clientJFrame.add(clientjScrollPane1);
                    clientDeleteActionPerformed(evt);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        clientClear.setText("Clear");
        clientClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clientClearBtnActionPerformed(evt);
            }
        });

    }
    private void clientjTable1MouseClicked(java.awt.event.MouseEvent evt) {
        int id = Integer.parseInt(clientjTable1.getValueAt(clientjTable1.getSelectedRow(), 1).toString());
        String nume = clientjTable1.getValueAt(clientjTable1.getSelectedRow(), 2).toString();
        String adresa = clientjTable1.getValueAt(clientjTable1.getSelectedRow(), 3).toString();
        idClient.setText(String.valueOf(id));
        numeClient.setText(nume);
        adresaClient.setText(adresa);
    }

    private void clientAddBtnActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        ClientBLL c = new ClientBLL();
        int id = Integer.parseInt(idClient.getText());
        String nume = numeClient.getText();
        String adresa = adresaClient.getText();
        Client client = new Client(id, nume, adresa);
        c.insert(client);
        retrieveClient();
    }

    private void clientUpdateBtnActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        int id = Integer.parseInt(idClient.getText());
        String numec = numeClient.getText();
        String adresa = adresaClient.getText();
        String[] options = {"Yes", "No"};
        int answ = JOptionPane.showOptionDialog(null, "Sure To Update??", "Update Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if (answ == 0) {
            Client c = new Client(id, numec, adresa);
            new ClientBLL().update(c);
            retrieveClient();
        }
    }

    private void clientDeleteActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        int id = Integer.parseInt(idClient.getText());
        String numec = numeClient.getText();
        String adresa = adresaClient.getText();
        String[] options = {"Yes", "No"};
        int answ = JOptionPane.showOptionDialog(null, "Sure To Delete??", "Delete Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if (answ == 0) {
            Client client = new Client(id, numec, adresa);
            new ClientBLL().delete(client);
            retrieveClient();
        }
    }

    private void clientClearBtnActionPerformed(java.awt.event.ActionEvent evt) {
        clientjTable1.setModel(new DefaultTableModel());
    }

    public void produs() {
        produsJFrame.add(produsjLabel1);
        produsJFrame.add(produsjLabel2);
        produsJFrame.add(produsjLabel3);
        produsJFrame.add(produsjLabel4);
        produsJFrame.add(idProdus);
        produsJFrame.add(numeProdus);
        produsJFrame.add(stocProdus);
        produsJFrame.add(pretProdus);
        produsJFrame.add(produsAdd);
        produsJFrame.add(produsUpdate);
        produsJFrame.add(produsRetrieve);
        produsJFrame.add(produsClear);
        produsJFrame.add(produsDelete);
        produsJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        produsJFrame.setSize(700, 500);
        produsJFrame.setLayout(null);
        produsJFrame.setVisible(true);
        produsJFrame.setTitle("Produs");

        produsjTable1.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                },
                new String [] {
                }
        ));
        produsjTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                produsjTable1MouseClicked(evt);
            }
        });

        produsjScrollPane1.setViewportView(produsjTable1);
        produsjLabel1.setText("idProdus");
        produsjLabel1.setBounds(450,40,100,50);
        produsjLabel2.setText("Nume");
        produsjLabel2.setBounds(450,80,100,50);
        produsjLabel3.setText("Pret");
        produsjLabel3.setBounds(450,120,100,50);
        produsjLabel4.setText("Stoc");
        produsjLabel4.setBounds(450,160,100,50);

        idProdus.setBounds(510,55,150,20);
        numeProdus.setBounds(510,95,150,20);
        stocProdus.setBounds(510,135,150,20);
        pretProdus.setBounds(510,175,150,20);

        produsAdd.setBounds(510,215,150,35);
        produsUpdate.setBounds(510,255,150,35);
        produsDelete.setBounds(510,295,150,35);
        produsRetrieve.setBounds(510,335,150,35);
        produsClear.setBounds(510,375,150,35);

        produsjTable1.setBounds(10,10,400,400);
        produsjScrollPane1.setBounds(10,10,400,400);

        produsRetrieve.setText("Retrieve");
        produsRetrieve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) produsjTable1.getModel();
                model.setRowCount(0);
                try {
                    retrieveProdus();
                    produsjScrollPane1.setViewportView(produsjTable1);

                    produsJFrame.add(produsjScrollPane1);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        produsAdd.setText("Add");
        produsAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) produsjTable1.getModel();
                model.setRowCount(0);
                try {
                    produsjScrollPane1.setViewportView(produsjTable1);

                    produsJFrame.add(produsjScrollPane1);
                    produsAddBtnActionPerformed(evt);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        produsUpdate.setText("Update");
        produsUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) produsjTable1.getModel();
                model.setRowCount(0);
                try {
                    produsjScrollPane1.setViewportView(produsjTable1);

                    produsJFrame.add(produsjScrollPane1);
                    produsUpdateBtnActionPerformed(evt);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        produsDelete.setText("Delete");
        produsDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) produsjTable1.getModel();
                model.setRowCount(0);
                try {
                    produsjScrollPane1.setViewportView(produsjTable1);

                    produsJFrame.add(produsjScrollPane1);
                    produsDeleteActionPerformed(evt);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        produsClear.setText("Clear");
        produsClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                produsClearBtnActionPerformed(evt);
            }
        });
    }

    private void produsjTable1MouseClicked(java.awt.event.MouseEvent evt) {
        int id = Integer.parseInt(produsjTable1.getValueAt(produsjTable1.getSelectedRow(), 1).toString());
        String nume = produsjTable1.getValueAt(produsjTable1.getSelectedRow(), 2).toString();
        int pret = Integer.parseInt(produsjTable1.getValueAt(produsjTable1.getSelectedRow(),3).toString());
        int stoc = Integer.parseInt(produsjTable1.getValueAt(produsjTable1.getSelectedRow(), 4).toString());
        idProdus.setText(String.valueOf(id));
        numeProdus.setText(nume);
        pretProdus.setText(String.valueOf(pret));
        stocProdus.setText(String.valueOf(stoc));
    }

    private void produsAddBtnActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        ProdusBLL c = new ProdusBLL();
        int id = Integer.parseInt(idProdus.getText());
        String nume = numeProdus.getText();
        int pret = Integer.parseInt(pretProdus.getText());
        int stoc = Integer.parseInt(stocProdus.getText());
        Produs produs = new Produs(id, nume, stoc, pret);
        c.insert(produs);
        retrieveProdus();
    }

    private void produsUpdateBtnActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        String[] options = {"Yes", "No"};
        int answ = JOptionPane.showOptionDialog(null, "Sure To Update??", "Update Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if (answ == 0) {
            int index = produsjTable1.getSelectedRow();
            Produs produs = new Produs(Integer.parseInt(idProdus.getText()), numeProdus.getText(), Integer.parseInt(stocProdus.getText()), Integer.parseInt(pretProdus.getText()));
            new ProdusBLL().update(produs);
            retrieveProdus();
        }
    }

    private void produsDeleteActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        String[] options = {"Yes", "No"};
        int answ = JOptionPane.showOptionDialog(null, "Sure To Delete??", "Delete Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if (answ == 0) {
            int index = produsjTable1.getSelectedRow();
            Produs produs = new Produs(Integer.parseInt(idProdus.getText()), numeProdus.getText(), Integer.parseInt(stocProdus.getText()), Integer.parseInt(pretProdus.getText()));
            new ProdusBLL().delete(produs);
            retrieveProdus();
        }
    }

    private void produsClearBtnActionPerformed(java.awt.event.ActionEvent evt) {
        produsjTable1.setModel(new DefaultTableModel());
    }

    public void comanda() {
        comandaJFrame.add(comandajLabel1);
        comandaJFrame.add(comandajLabel2);
        comandaJFrame.add(comandajLabel3);
        comandaJFrame.add(comandajLabel4);
        comandaJFrame.add(idComanda);
        comandaJFrame.add(cantitateComanda);
        comandaJFrame.add(numeClientComada);
        comandaJFrame.add(comandaAdd);
        comandaJFrame.add(comandaUpdate);
        comandaJFrame.add(comandaRetrieve);
        comandaJFrame.add(comandaClear);
        comandaJFrame.add(comandaDelete);
        comandaJFrame.add(comandajComboBox);
        comandaJFrame.add(comandaClientCombo);
        comandaJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        comandaJFrame.setSize(700, 500);
        comandaJFrame.setLayout(null);
        comandaJFrame.setVisible(true);
        comandaJFrame.setTitle("Comanda");

        comandajTable1.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                },
                new String [] {
                }
        ));
        comandajTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                comandajTable1MouseClicked(evt);
            }
        });

        comandajScrollPane1.setViewportView(comandajTable1);
        comandajLabel1.setText("idComanda");
        comandajLabel1.setBounds(430,40,100,50);
        comandajLabel2.setText("NumeClient");
        comandajLabel2.setBounds(430,80,100,50);
        comandajLabel3.setText("NumeProdus");
        comandajLabel3.setBounds(430,120,100,50);
        comandajLabel4.setText("Cantitate");
        comandajLabel4.setBounds(430,160,100,50);

        idComanda.setBounds(510,55,150,20);
        comandaClientCombo.setBounds(510,95,150,20);
        comandajComboBox.setBounds(510,135,150,20);
        cantitateComanda.setBounds(510,175,150,20);

        comandaAdd.setBounds(510,215,150,35);
        comandaUpdate.setBounds(510,255,150,35);
        comandaDelete.setBounds(510,295,150,35);
        comandaRetrieve.setBounds(510,335,150,35);
        comandaClear.setBounds(510,375,150,35);

        comandajTable1.setBounds(10,10,400,400);
        comandajScrollPane1.setBounds(10,10,400,400);

        comandaRetrieve.setText("Retrieve");
        comandaRetrieve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) comandajTable1.getModel();
                model.setRowCount(0);
                try {
                    retrieveComanda();
                    comandajScrollPane1.setViewportView(comandajTable1);

                    comandaJFrame.add(comandajScrollPane1);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        comandaAdd.setText("Add");
        comandaAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) comandajTable1.getModel();
                model.setRowCount(0);
                comandajScrollPane1.setViewportView(comandajTable1);

                comandaJFrame.add(comandajScrollPane1);
                comandaAddBtnActionPerformed(evt);
            }
        });

        comandaUpdate.setText("Update");
        comandaUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) comandajTable1.getModel();
                model.setRowCount(0);
                try {
                    comandajScrollPane1.setViewportView(comandajTable1);

                    comandaJFrame.add(comandajScrollPane1);
                    comandaUpdateBtnActionPerformed(evt);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        comandaDelete.setText("Delete");
        comandaDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DefaultTableModel model = (DefaultTableModel) comandajTable1.getModel();
                model.setRowCount(0);
                try {
                    comandajScrollPane1.setViewportView(comandajTable1);

                    comandaJFrame.add(comandajScrollPane1);
                    comandaDeleteActionPerformed(evt);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

        comandaClear.setText("Clear");
        comandaClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comandaClearBtnActionPerformed(evt);
            }
        });

        produsCombo();
        clientCombo();

    }

    private void comandajTable1MouseClicked(java.awt.event.MouseEvent evt) {
        int id = Integer.parseInt(comandajTable1.getValueAt(comandajTable1.getSelectedRow(), 1).toString());
        String numeClient = comandajTable1.getValueAt(comandajTable1.getSelectedRow(), 2).toString();
        String numeProdus = comandajTable1.getValueAt(comandajTable1.getSelectedRow(),3).toString();
        int cantitate = Integer.parseInt(comandajTable1.getValueAt(comandajTable1.getSelectedRow(), 4).toString());
        idComanda.setText(String.valueOf(id));
        numeClientComada.setText(numeClient);
        numeProdusComanda.setText(numeProdus);
        cantitateComanda.setText(String.valueOf(cantitate));
    }

    private void comandaRetrieveBtnActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        retrieveComanda();
    }

    private void comandaAddBtnActionPerformed(java.awt.event.ActionEvent evt) {
        int id = Integer.parseInt(idComanda.getText());
        String nume = comandaClientCombo.getSelectedItem().toString();
        String numep = comandajComboBox.getSelectedItem().toString();
        int cantitate = Integer.parseInt(cantitateComanda.getText());
        ProdusDAO p = new ProdusDAO();
        if (p.findbyname(numep).getStoc() < cantitate) {
            JOptionPane.showMessageDialog(null, "Stoc insuficient", "", JOptionPane.INFORMATION_MESSAGE);
        } else {
            Produs p1 = new Produs();
            p1 = p.findbyname(numep);
            ComandaBLL cd1 = new ComandaBLL();
            Comanda comanda = new Comanda(id, nume, numep, cantitate);
            try {
                cd1.insert(comanda);
                retrieveComanda();
            }catch (Exception e){
                e.printStackTrace();
            }
            p.editareStoc(p1.getStoc()-cantitate, numep);
        }
    }

    private void comandaUpdateBtnActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        int id = Integer.parseInt(idComanda.getText());
        String nume = comandaClientCombo.getSelectedItem().toString();
        String numep = comandajComboBox.getSelectedItem().toString();
        int cantitate = Integer.parseInt(cantitateComanda.getText());
        ProdusDAO p = new ProdusDAO();
        if (p.findbyname(numep).getStoc() < cantitate) {
            JOptionPane.showMessageDialog(null, "Stoc insuficient", "", JOptionPane.INFORMATION_MESSAGE);
        }else {
            Produs p1 = new Produs();
            p1 = p.findbyname(numep);
            String[] options = {"Yes", "No"};
            int answ = JOptionPane.showOptionDialog(null, "Sure To Update??", "Update Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if (answ == 0) {
                Comanda comanda = new Comanda(id, nume, numep, cantitate);
                new ComandaBLL().update(comanda);
                retrieveComanda();
            }
            p.editareStoc(p1.getStoc()-cantitate, numep);
        }
    }

    private void comandaDeleteActionPerformed(java.awt.event.ActionEvent evt) throws IllegalAccessException {
        String[] options = {"Yes", "No"};
        int answ = JOptionPane.showOptionDialog(null, "Sure To Delete??", "Delete Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if (answ == 0) {
            int index = comandajTable1.getSelectedRow();
            String nume = comandaClientCombo.getSelectedItem().toString();
            String numep = comandajComboBox.getSelectedItem().toString();
            Comanda comanda = new Comanda(Integer.parseInt(idComanda.getText()), nume, numep, Integer.parseInt(cantitateComanda.getText()));
            new ComandaBLL().delete(comanda);
            retrieveComanda();
        }
    }

    private void comandaClearBtnActionPerformed(java.awt.event.ActionEvent evt) {
        comandajTable1.setModel(new DefaultTableModel());
    }

    private void produsCombo(){
        Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try{
            con = ConnectionFactory.getConnection();
            String sql = "SELECT * FROM produs";
            pst = con.prepareStatement(sql);
            rs=pst.executeQuery();
            while(rs.next()){
                String name = rs.getString("nume");
                comandajComboBox.addItem(name);
            }
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
    private void clientCombo(){
        Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try{
            con = ConnectionFactory.getConnection();
            String sql = "SELECT * FROM client";
            pst = con.prepareStatement(sql);
            rs=pst.executeQuery();
            while(rs.next()){
                String name = rs.getString("nume");
                comandaClientCombo.addItem(name);
            }
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
    }


    private void produsBtnActionPerformed(java.awt.event.ActionEvent evt){
        if (evt.getSource() == produsBtn) {
            produsJFrame.dispose();
            produs();
        }
    }

    private void clientBtnActionPerformed(java.awt.event.ActionEvent evt){
        if (evt.getSource() == clientBtn) {
            clientJFrame.dispose();
            client();
        }
    }

    private void comandaBtnActionPerformed(java.awt.event.ActionEvent evt){
        if (evt.getSource() == comandaBtn) {
            comandaJFrame.dispose();
            comanda();
        }
    }
    private javax.swing.JFrame jframe;
    private javax.swing.JButton produsBtn;
    private javax.swing.JFrame produsJFrame;
    private javax.swing.JButton produsAdd;
    private javax.swing.JButton produsUpdate;
    private javax.swing.JButton produsDelete;
    private javax.swing.JButton produsClear;
    private javax.swing.JButton produsRetrieve;
    private javax.swing.JLabel produsjLabel1;
    private javax.swing.JLabel produsjLabel2;
    private javax.swing.JLabel produsjLabel3;
    private javax.swing.JLabel produsjLabel4;
    private javax.swing.JScrollPane produsjScrollPane1;
    private javax.swing.JTable produsjTable1;
    private javax.swing.JTextField numeProdus;
    private javax.swing.JTextField stocProdus;
    private javax.swing.JTextField idProdus;
    private javax.swing.JTextField pretProdus;

    private javax.swing.JButton clientBtn;
    private javax.swing.JFrame clientJFrame;
    private javax.swing.JButton clientDelete;
    private javax.swing.JButton clientAdd;
    private javax.swing.JButton clientClear;
    private javax.swing.JButton clientRetrieve;
    private javax.swing.JButton clientUpdate;
    private javax.swing.JLabel clientjLabel1;
    private javax.swing.JLabel clientjLabel2;
    private javax.swing.JLabel clientjLabel3;
    private javax.swing.JLabel clientjLabel4;
    private javax.swing.JScrollPane clientjScrollPane1;
    private javax.swing.JTable clientjTable1;
    private javax.swing.JTextField numeClient;
    private javax.swing.JTextField idClient;
    private javax.swing.JTextField adresaClient;

    private javax.swing.JButton comandaBtn;
    private javax.swing.JFrame comandaJFrame;
    private javax.swing.JButton comandaDelete;
    private javax.swing.JButton comandaAdd;
    private javax.swing.JButton comandaClear;
    private javax.swing.JButton comandaRetrieve;
    private javax.swing.JButton comandaUpdate;
    private javax.swing.JLabel comandajLabel1;
    private javax.swing.JLabel comandajLabel2;
    private javax.swing.JLabel comandajLabel3;
    private javax.swing.JLabel comandajLabel4;
    private javax.swing.JScrollPane comandajScrollPane1;
    private javax.swing.JTable comandajTable1;
    private javax.swing.JTextField numeClientComada;
    private javax.swing.JTextField idComanda;
    private javax.swing.JTextField numeProdusComanda;
    private javax.swing.JTextField cantitateComanda;
    private javax.swing.JComboBox comandajComboBox;
    private javax.swing.JComboBox comandaClientCombo;
}

