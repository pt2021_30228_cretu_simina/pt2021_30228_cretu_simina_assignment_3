package ro.tuc.tp.start;

import ro.tuc.tp.presentation.View;

/**
 * This class implements the main method
 */
public class Start {
    public static void main( String[] args ){
        new View().setVisible(true);
    }
}